'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('DashboardCtrl', function($scope, $rootScope, $state, $timeout, $http, AuthService) {

    $scope.$state = $state;
    
    $scope.showDash = true;
    $scope.hideDash = true;
  
  $scope.showClosedDash = function(){
     $scope.showDash = true;
      
     $timeout(function(){$scope.hideDash = true;},500);
  };
  
  $scope.logout = function(){

  	$http({
  		method: 'GET',
  		url: $rootScope.apiurl + '/logout'
  	});

  	$.removeCookie('gpsnav_api');

  	AuthService.setApi('');
  	AuthService.setLogged(false);

  	$state.go('login');

  };

  });
