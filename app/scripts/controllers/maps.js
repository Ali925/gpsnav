'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('MapsCtrl', function($scope, $rootScope, $state, $timeout, $http, AuthService) {

  	var api = AuthService.getApi();
  	$scope.userType = AuthService.getUserType();
  	
  	$scope.map = {};

    $scope.map.selectedCourier = {};
    $scope.map.selectedDate = {};

    $scope.map.center = {
      lat: 0,
      lng: 0,
      zoom: 0
    };
  	$scope.map.tiles = {};
  	$scope.map.height = $(window).height()*0.82 + 'px';

  	var auth_token = 'pk.eyJ1IjoiYWxpOTI1IiwiYSI6ImNpcHZobHIxMzAwNTZpMWtzcmFrdmk2OXcifQ.7AB_LOCsVI6kUn_vPDUXkg';

  	$scope.map.tiles.url = 'https://api.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=' + auth_token;

  	$scope.map.initMap = function(){

  		$('.closed-sidebar + .main').addClass('no-scroll');
  	};

    $scope.map.paths = {
          p1: {
                color: '#008000',
                weight: 8,
                latlngs: [],
            }
        };

    $scope.map.markers = {};

    $scope.map.startCouriers = function(){

      $http({
          method: 'GET',
          url: $rootScope.apiurl + '/get/list/courier',
          params: {api_token: api}
        }).then(function successCallback(response){
            console.log(response);
            $scope.map.couriersList = response.data.message;

            for(var i in $scope.map.couriersList){

              $scope.map.couriersList[i].fullName = $scope.map.couriersList[i].username + ' (' + $scope.map.couriersList[i].last_name + ' ' + $scope.map.couriersList[i].first_name + ' ' + $scope.map.couriersList[i].middle_name + ')';
              }
        });
    };

  	$scope.map.updateCoords = function(){

      $scope.map.selectedDate = {};

  		$http({
  			method: 'GET',
  			url: $rootScope.apiurl + '/get/list/coordinates',
  			params: {api_token: api, user_id: $scope.map.selectedCourier.id}
  		}).then(function(response){

        var coords = response.data.message;
        $scope.map.coordinates = [];
        var latitudes = [], longitudes = [], date, time, oldDate, oldLat, oldLng, coordNum = 0, latlngs = [];

  			for(var coord in coords){

            date = coords[coord].created_at.substring(0, 10);
            time = coords[coord].created_at.substring(11);

            if(date!==oldDate && coord!=0 && latlngs.length){
              latitudes = [];
              longitudes = [];
              latlngs = [];
              coordNum++;
            }
            
            
            if(parseFloat(coords[coord].latitude))
              latitudes.push(parseFloat(coords[coord].latitude));
            if(parseFloat(coords[coord].longitude))
              longitudes.push(parseFloat(coords[coord].longitude));
            if(parseFloat(coords[coord].latitude) && parseFloat(coords[coord].longitude)){
              latlngs.push({
                lat: parseFloat(coords[coord].latitude),
                lng: parseFloat(coords[coord].longitude),
                time: time
              });

            $scope.map.coordinates[coordNum] = {
              date: date,
              lat: latitudes,
              lng: longitudes,
              latlngs: latlngs,
              id: coordNum
            };
            
            }

            oldDate = date;
        }

        console.log($scope.map.coordinates);

  		});
  	};

    $scope.map.updateDates = function(){

      $scope.map.paths = {
          p1: {
                color: '#d9534f',
                weight: 8,
                latlngs: [],
            }
        };

    $scope.map.markers = {};

      var dateNum = $scope.map.selectedDate.id, diffTime = 0;

      $scope.map.center.lat = (Math.min.apply(null, $scope.map.coordinates[dateNum].lat)+Math.max.apply(null, $scope.map.coordinates[dateNum].lat))/2;
        $scope.map.center.lng = (Math.min.apply(null, $scope.map.coordinates[dateNum].lng)+Math.max.apply(null, $scope.map.coordinates[dateNum].lng))/2;
        $scope.map.center.zoom = 15;

        $scope.map.paths.p1.latlngs = $scope.map.coordinates[dateNum].latlngs;

        for(var coord in $scope.map.coordinates[dateNum].latlngs){
          if (coord==0) {
            $scope.map.markers['marker' + coord] = $scope.map.coordinates[dateNum].latlngs[coord];
            $scope.map.markers['marker' + coord].message = "Начало<br>" + $scope.map.coordinates[dateNum].latlngs[coord].time;
          }
          else if(coord == $scope.map.coordinates[dateNum].latlngs.length-1){
            $scope.map.markers['marker' + coord] = $scope.map.coordinates[dateNum].latlngs[coord];
            $scope.map.markers['marker' + coord].message = "Конец<br>" + $scope.map.coordinates[dateNum].latlngs[coord].time;
          }
          else{
            var icon = {};
            icon.iconSize = [40, 40];
            icon.iconAnchor = [10, 10];
            icon.type = 'div';
            

            if($scope.map.coordinates[dateNum].latlngs[coord].lat === $scope.map.coordinates[dateNum].latlngs[parseInt(coord)+1].lat && $scope.map.coordinates[dateNum].latlngs[coord].lng === $scope.map.coordinates[dateNum].latlngs[parseInt(coord)+1].lng){
                diffTime += (timeToSeconds($scope.map.coordinates[dateNum].latlngs[coord].time) - timeToSeconds($scope.map.coordinates[dateNum].latlngs[parseInt(coord)-1].time));  
            }
            else if($scope.map.coordinates[dateNum].latlngs[coord].lat === $scope.map.coordinates[dateNum].latlngs[parseInt(coord)-1].lat && $scope.map.coordinates[dateNum].latlngs[coord].lng === $scope.map.coordinates[dateNum].latlngs[parseInt(coord)-1].lng){
              
              diffTime += (timeToSeconds($scope.map.coordinates[dateNum].latlngs[coord].time) - timeToSeconds($scope.map.coordinates[dateNum].latlngs[parseInt(coord)-1].time));
              icon.html = "<i class='fa fa-clock-o' aria-hidden='true' style = 'color:white;font-size: 25px; -webkit-transform: rotate("+ 0 +"deg)'></i>";
              
              var delay = secondsToTime(diffTime);

              $scope.map.markers['marker' + coord] = $scope.map.coordinates[dateNum].latlngs[coord];
              $scope.map.markers['marker' + coord].icon = icon;
              $scope.map.markers['marker' + coord].message = 'Приостановка: ' + delay + "<br>" + $scope.map.coordinates[dateNum].latlngs[coord].time;

              diffTime = 0;

            }
            else{

              var diffLat = $scope.map.coordinates[dateNum].latlngs[parseInt(coord)+1].lat - $scope.map.coordinates[dateNum].latlngs[coord].lat;
              var diffLng = $scope.map.coordinates[dateNum].latlngs[parseInt(coord)+1].lng - $scope.map.coordinates[dateNum].latlngs[coord].lng;
              var angle = 360 - (Math.atan2(diffLat, diffLng)*57.295779513082);

              icon.html = "<i class='fa fa-arrow-right' aria-hidden='true' style = 'color:black;font-size: 20px; -webkit-transform: rotate("+ angle +"deg)'></i>";
              
              $scope.map.markers['marker' + coord] = $scope.map.coordinates[dateNum].latlngs[coord];
              $scope.map.markers['marker' + coord].icon = icon;
              $scope.map.markers['marker' + coord].message = $scope.map.coordinates[dateNum].latlngs[coord].time;

            }
     
          }
        }
    };

    function timeToSeconds(time){
      var hours = parseInt(time.substring(0, 2));
      var mins = parseInt(time.substring(3, 5));
      var secs = parseInt(time.substring(6));

      return secs+(mins*60)+(hours*3600);
    }

    function secondsToTime(seconds){
      var hours = parseInt(seconds/3600);
      var mins = parseInt((seconds-hours*3600)/60);
      var secs = parseInt(seconds-hours*3600-mins*60);

      if(hours<10)
        hours = '0' + hours;
      if(mins<10)
        mins = '0' + mins;
      if(secs<10)
        secs = '0' + secs;

      return hours + ':' + mins + ':' + secs;
    }

  });